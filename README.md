# axios入门与源码解析
## json-server搭建
* `npm i -g json-server`全局安装服务
* 在`db.json`文件所在目录执行`json-server --watch db.json`此时db.json就称为了后台服务数据库，该命令执行成功会返回地址，通过地址可以模拟接口获取响应数据
* `http://localhost:3000/posts/1`获取相对应的某一条数据
## 一些相关知识
### 区别一般http和ajax
1. ajax请求是一种特别的http请求
2. 对服务器来说没有任何区别，区别在浏览器端
3. 浏览器端发请求：只有XHR或fetch发出的才是ajax请求，其他所有的都是非ajax请求
4. 浏览器端接收响应
  * 一般请求：浏览器一般会直接显示响应数据，也就是我们常说的刷新/跳转页面
  * ajax请求：浏览器不会对界面进行任何更新操作，只是调用监视的回调函数并传入响应相关数据
## 源码分析
### node_modules-->axios-->lib文件夹（核心目录）
#### adapters定义请求的适配器xhr、http
* http.js--实现http适配器（包装http包）实现服务端请求接口
* xhr.js--实现xhr适配器（包装xhr对象）实现前端发送ajax请求
#### cancel定义取消功能
* Cancel.js--构造函数用来创建取消时的错误对象
* CancelToken.js--创建取消请求的对象的构造函数
* isCancel.js--判断参数是否为取消对象
#### core一些核心功能
  * Axios.js--axios的核心主类创建Axios的构造函数
  * buildFullPath.js--构造完整的url的函数文件
  * createError.js--创建error对象
  * dispatchRequest.js--用来调用http请求适配器方法发送请求的函数（发送请求的）由他去调用http.js或xhr.js发起请求
  * enhanceError.js--更新错误对象的函数文件
  * InterceptorManger.js--拦截器管理器的构造函数
  * mergeConfig.js--合并配置的文件
  * settle.js--根据响应的状态码来修改promise的状态
  * transformData.js--对结果进行格式化
#### helpers里面存放功能函数
* bind.js--收到一个函数，创建一个新函数将新函数this绑定到一个对象身上
* buildURl.js--创建URL可以在后面放置参数
* combineURLs.ja--合并URL函数暴露形成最终的URL
* cookies.js--对cookie进行操作
* deprecatedMethod.js--控制台提示不赞成使用的方法
* isAbsoluteURL.js--判断URL是不是绝对路径
* isAxiosErroe.js--
* isURLSameOrigin.js--判断URL是不是是不是同源
* normallizeHeaderName.js--统一化信息，统一变为大写
* parseHeaders.js--对头信息进行解析
* spread.js--对请求坐批量处理
#### axios.js---axios入口文件
#### default.js---axios默认配置文件
#### utils.js---工具函数文件
#### index.js---整个包的入口文件

### axios请求过程
* 前台axios->调用request方法->调用dispatch方法->xhr请求
* 前台通过axios开始发起请求，axios调用request方法，request方法又调用dispatch方法,dispatch是适配器会决定调用http还是xhr发起请求，xhr调用成功返回的是一个对象，而dispatch返回的是一个promise对象，由于xhr返回的是对象所以dispatch返回成功的promise状态，而dispatch的返回会使得request方法的promise对象成功状态，最终会执行我们前台写的axios().then()成功里面的回调。
### axios拦截器工作原理
* `axios.interceptors.request.use(function(){成功回调},function(){失败回调})`请求拦截器-1号
* `axios.interceptors.request.use(function(){成功回调},function(){失败回调})`请求拦截器-2号
* `axios.interceptors.response.use(function(){成功回调},function(){失败回调})`响应拦截器-1号
* `axios.interceptors.response.use(function(){成功回调},function(){失败回调})`响应拦截器-2号
1. axios对象中有`interceptors`属性:`this.interceptors = {request: new InterceptorManager(),response: new InterceptorManager()}`而`new InterceptorManager()`会生成`handlers[]`属性。
2. 对于请求拦截器会向`request`中的`handlers`中依次push请求拦截器的成功与失败回调，每个拦截器为一个对象生成`[{请求拦截器1的成功回调，请求拦截器1的失败回调},{请求拦截器2的成功回调，请求拦截器2的失败回调}]`；
3. 对于响应拦截器也同样会向`response`中的`handlers`依次push响应拦截器生成`[{响应拦截器1的成功回调，响应拦截器1的失败回调},{响应拦截器2的成功回调，响应拦截器2的失败回调}]`最终`request`和`response`中都会生成handers数组。
4. 接下来axios在`request`发送请求的时候有`var chain = [dispatchRequest, undefined];`其中`dispatchRequest`就是发送请求的函数，axios会执行`this.interceptors.request.forEach`这个方法是axios自己封装的，会遍历`request`上的`handers`数组并执行`chain.unshift(interceptor.fulfilled, interceptor.rejected);`将`handers`中的元素通过`unshift`方式追加到`chain`中（`chain`初始为`[dispatchRequest, undefined]`）此时`chain`就变成了`[请求拦截2号成功回调,请求拦截2号失败回调,请求拦截1号成功回调,请求拦截1号失败回调,dispatchRequest, undefined]`；
5. 之后对于响应拦截也同样会执行`this.interceptors.response.forEach`遍历`response`上的`handers`数组并执行`chain.push(interceptor.fulfilled, interceptor.rejected);`将`handers`中的元素通过`push`方式追加到`chain`中，此时`chain`会变成 `[请求拦截2号成功回调,请求拦截2号失败回调,请求拦截1号成功回调,请求拦截1号失败回调,dispatchRequest, undefined,响应拦截器1号成功回调,响应拦截器1号失败回调,响应拦截器2号成功回调,响应拦截器2号失败回调]`
6. 之后会执行`while(chain.length){promise=promise.then(chain.shift(),chain.shift());}`此时`chain`中的回调执行顺序为：`请求拦截2号成功回调->请求拦截1号成功回调->dispatchRequest->响应拦截器1号成功回调->响应拦截器2号成功回调->前端的成功回调`其中每个函数返回的都是promise对象，如果哪一步返回是失败那么下一步就不会再执行成功回调就会执行响应的失败回调最终调用前端的失败回调逻辑
### 源码分析总结
#### axios与Axios的关系
* 从语法上来说：axios不是Axios的实例
* 从功能上来说：axios是Axios的实例
* axios是Axios.prototype.request函数bind()返回的函数
* axios作为对象有Axios原型对象上的所有方法，有Axios对象上所有属性
#### instance(create创建的实例对象)与axios的区别
* 相同：
  1. 都是一个能发任意请求的函数request(config)
  2. 都有发特定请求的各种方法：get()/post()/pull()/delete()
  3. 都有默认配置和拦截器的属性：defaults/interceptors
* 不同
  1. 默认配置很可能不一样
  2. instance没有axios后面添加的一些方法：create()/CancelToken()/all()
#### axios运行整体流程
```javascript
                  axios          axios.create()
                    |                  |
                    -------------------
                            |
                    createInstance()
                            |
                  config--->执行/别名执行
                            |
                  Axios.prototype.request
                        request
                      interceptors
                      （请求拦截器）
                            |
    处理参数与默认参数/     
    transformdata --> dispatchRequest
                            |
                         adapter
                            |
    axios rejected <-- 报错/cancel --> axios fulfilled
                            |
                        response 
                      interceptors
                            |
                    请求的onResolved
                      或onRejected

```
#### 拦截器
1. 请求拦截器
  * 在真正发送请求前执行的回调函数
  * 可以对请求进行检查或配置进行待定处理
  * 成功的回调函数，传递的默认是config(也必须是)
  * 失败的回调函数，传递的默认是error
2. 响应拦截器
  * 在请求得到响应后执行的回调函数
  * 可以对响应数据进行特定的处理
  * 成功的回调函数，传递的默认是response
  * 失败的回调函数，传递的默认是error
#### 如何取消未完成的请求
1. 当配置了cancelToken对象时，保存cancel函数
  (1) 创建一个用于将来中断请求的cancelPromise
  (2) 并定义了一个用于取消请求的cancel函数
  (3) 将cancel函数传递出来
2. 调用cancel()取消请求
  (1) 执行cancel函数，传入错误信息message
  (2) 内部会让cancelPromise变为成功，且成功的值为一个Cancel对象
  (3) 在cancelPromise的成功回调中中断请求，并让发请求的promise失败，失败的reason为Cancel对象